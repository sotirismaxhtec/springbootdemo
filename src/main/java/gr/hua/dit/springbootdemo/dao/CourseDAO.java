package gr.hua.dit.springbootdemo.dao;

import gr.hua.dit.springbootdemo.entity.Course;

import java.util.List;

public interface CourseDAO {

    public List<Course> findAll();
    public void save(Course course);

    public Course findById(int id);
}
