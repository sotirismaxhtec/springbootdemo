package gr.hua.dit.springbootdemo.dao;

import gr.hua.dit.springbootdemo.entity.Course;
import gr.hua.dit.springbootdemo.entity.Teacher;

import java.util.List;

public interface TeacherDAO {

    public List<Teacher> findAll();
    public void save(Teacher teacher);

    public Teacher findById(int id);

    public void delete(int id);
}
