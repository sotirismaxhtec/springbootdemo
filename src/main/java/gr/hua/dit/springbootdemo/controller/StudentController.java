package gr.hua.dit.springbootdemo.controller;

import gr.hua.dit.springbootdemo.entity.Student;
import gr.hua.dit.springbootdemo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    StudentRepository studentRepository;

    @GetMapping("")
    public List<Student> getAll()
    {
        return studentRepository.findAll();
    }
}
